<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome</title>
</head>
<body>
	<h3><strong>Welcome, <s:property value="ownerName" /></strong> </h3>
	<s:url id="deptUrl" action="dept_redirect"> </s:url>
	<s:a href="%{deptUrl}">Department</s:a>
	<br/>
	<s:url id="emplUrl" action="empl_redirect"> </s:url>
	<s:a href="%{emplUrl}">Employee</s:a>
</body>
</html>