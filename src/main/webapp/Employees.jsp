<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<sx:head />
<title>Employees</title>
</head>

<style type="text/css">
	@import url(style.css);
</style>
<body>
	<strong>ADD NEW EMPLOYEE</strong>
	<s:form action="create_employee">
		<s:textfield key="firstName" name="employee.firstName"/>
		<s:textfield key="lastName" name="employee.lastName"/>
		<s:textfield key="email" name="employee.email"/>
		<s:textfield key="phoneNumber" name="employee.phoneNumber"/>
		<sx:datetimepicker name="employee.hireDate" key="hireDate" displayFormat="MM/dd/yy" value="currentDate"/>
		<s:select key="jobId" list="jobs" headerKey="-1" headerValue="Select job..." name="employee.jobId" listKey="jobId" listValue="jobTittle" />
		<s:textfield key="salary" name="employee.salary" />
		<s:textfield key="commissionPct" name="employee.commissionPct" />
		<s:select key="managerId" list="managers" headerKey="-1" headerValue="Select manager..." name="employee.managerId" listKey="employeeId" listValue="fullName" />
		<s:select key="dept" list="departments" headerKey="-1" headerValue="Select department..." name="employee.departmentId" listKey="departmentId" listValue="departmentName" />
		<s:submit value="Submit"/>
	</s:form>
	<div class="content">
		<table class="emplTable" cellpadding="5px">
			<tr class="even">
				<th>Employee Id</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th>
				<th>Phone Number</th>
				<th>Hire Date</th>
				<th>Job Id</th>
				<th>Salary</th>
				<th>Commission Pct</th>
				<th>Manager Id</th>
				<th>Department Id</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
			<s:if test="employees.size() > 0">
				<s:iterator value="employees" status="employeeStatus">
					<tr
						class="<s:if test="#employeeStatus.odd == true ">odd</s:if> <s:else>even</s:else>">
						
						<td><s:property value="employeeId"/></td>
						<td><s:property value="firstName"/></td>
						<td><s:property value="lastName"/></td>
						<td><s:property value="email"/></td>
						<td><s:property value="phoneNumber" /></td>
						<td><s:property value="hireDate" /></td>
						<td><s:property value="jobId" /></td>
						<td><s:property value="salary" /></td>
						<td><s:property value="commissionPct" /></td>
						<td><s:property value="managerId" /></td>
						<td><s:property value="departmentId" /></td>
						<td>
		                <s:url id="editURL" action="edit_employee_redirect">
							<s:param name="id" value="%{employeeId}"></s:param>
						</s:url>
		                <s:a href="%{editURL}">Edit</s:a>
		                </td>
		                
						<td>
		                <s:url id="deleteURL" action="delete_employee">
							<s:param name="id" value="%{employeeId}"></s:param>
						</s:url>
		                <s:a href="%{deleteURL}">Delete</s:a>
		                </td>
		                
					</tr>
				</s:iterator>
			</s:if>
		</table>
		</div>
</body>
</html>