<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<sx:head />
<title>Insert title here</title>
</head>
<body>
	<h1>EDIT NEW DEPARTMENT</h1>
	<s:form action="edit_employee">
		<s:textfield key="employeeId" name="employee.employeeId" readonly="true"/>
		<s:textfield key="firstName" name="employee.firstName"/>
		<s:textfield key="lastName" name="employee.lastName"/>
		<s:textfield key="email" name="employee.email"/>
		<s:textfield key="phoneNumber" name="employee.phoneNumber"/>
		<sx:datetimepicker name="employee.hireDate" key="hireDate" displayFormat="MM/dd/yy" value="employee.hireDate"/>
		<s:select key="jobId" list="jobs" headerKey="-1" headerValue="Select job..." name="employee.jobId" listKey="jobId" listValue="jobTittle" />
		<s:textfield key="salary" name="employee.salary" />
		<s:textfield key="commissionPct" name="employee.commissionPct" />
		<s:select key="managerId" list="managers" headerKey="-1" headerValue="Select manager..." name="employee.managerId" listKey="employeeId" listValue="fullName" />
		<s:select key="dept" list="departments" headerKey="-1" headerValue="Select department..." name="employee.departmentId" listKey="departmentId" listValue="departmentName" />
		<s:submit value="Submit"/>
	</s:form>
</body>
</html>