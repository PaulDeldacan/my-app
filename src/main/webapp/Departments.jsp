<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Departments</title>
</head>

<style type="text/css">
	@import url(style.css);
</style>

<body>
	<strong>ADD NEW DEPARTMENT</strong>
	<s:form action="create_dept">
		<s:textfield key="deptName" name="department.departmentName"/>
		<s:submit value="Add"/>
	</s:form>

	<div class="content">
		<table class="deptTable" cellpadding="5px">
			<tr class="even">
				<th>Department Id</th>
				<th>Department Name</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
			<s:if test="departments.size() > 0">
				<s:iterator value="departments" status="deptStatus">
					<tr
						class="<s:if test="#deptStatus.odd == true ">odd</s:if> <s:else>even</s:else>">
						
						<td><s:property value="departmentId" /></td>
						<td><s:property value="departmentName" /></td>
						
						<td>
		                <s:url id="editURL" action="edit_dept_redirect">
							<s:param name="id" value="%{departmentId}"></s:param>
						</s:url>
		                <s:a href="%{editURL}">Edit</s:a>
		                </td>
		                
						<td>
		                <s:url id="deleteURL" action="delete_dept">
							<s:param name="id" value="%{departmentId}"></s:param>
						</s:url>
		                <s:a href="%{deleteURL}">Delete</s:a>
		                </td>
		                
					</tr>
				</s:iterator>
			</s:if>
		</table>
		</div>
</body>
</html>