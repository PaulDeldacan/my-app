package com.company.injector;

import com.company.daos.DepartmentDao;
import com.company.daos.DepartmentDaoImpl;
import com.company.daos.EmployeeDao;
import com.company.daos.EmployeeDaoImpl;
import com.company.daos.JobDao;
import com.company.daos.JobDaoImpl;
import com.company.services.DepartmentService;
import com.company.services.DepartmentServiceImpl;
import com.company.services.EmployeeService;
import com.company.services.EmployeeServiceImpl;
import com.company.services.JobService;
import com.company.services.JobServiceImpl;
import com.google.inject.AbstractModule;

public class AppInjector extends AbstractModule {

	@Override
	protected void configure() {
		bind( DepartmentDao.class ).to( DepartmentDaoImpl.class  );
		bind( DepartmentService.class ).to( DepartmentServiceImpl.class );
		bind( EmployeeDao.class ).to( EmployeeDaoImpl.class );
		bind( EmployeeService.class ).to( EmployeeServiceImpl.class );
		bind( JobDao.class ).to( JobDaoImpl.class );
		bind( JobService.class ).to( JobServiceImpl.class );
	}

}
