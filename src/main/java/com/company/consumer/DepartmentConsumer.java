package com.company.consumer;

import java.util.List;

import javax.inject.Inject;

import com.company.daos.DepartmentDao;
import com.company.models.Department;

public class DepartmentConsumer {
	private DepartmentDao service;
	
	@Inject
	public DepartmentConsumer( DepartmentDao service ){
		setService( service );
	}
	
	public void setService( DepartmentDao service ){
		this.service = service;
	}
	
	private DepartmentDao getService(){
		return this.service;
	}
	
	public boolean saveDepartment( Department department ){
		return getService().save( department );
	}
	
	public List<Department> getAllDepartments(){
		List<Department> depts = getService().selectAll();
		if ( depts != null ) { return depts; }
		else{ return null; }
	}
	
	public Department getDeptById( int id ){
		Department dept = getService().getObjectById( id );
		return dept;
	}
	
	public boolean deleteDepartment( Department t ){
		return getService().delete( t );
	}
	
	public boolean updateDepartment( Department t ){
		return getService().merge( t );
	}
}
