package com.company.daos;

import com.company.models.Department;

public interface DepartmentDao extends DbDao<Department> {

}
