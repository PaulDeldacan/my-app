package com.company.daos;

import java.util.List;

public interface DbDao<T> {
	boolean save(T t);
	boolean merge(T t);
	boolean delete(T t); 
	List<T> selectAll();
	T getObjectById( int id );
}
