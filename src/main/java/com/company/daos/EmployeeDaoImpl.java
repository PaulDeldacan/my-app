package com.company.daos;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.company.models.Employee;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class EmployeeDaoImpl implements EmployeeDao{

	@Inject
	SqlSession session;
	
	private SqlSession getSqlSession(){
		return this.session;
	}
	
	public boolean save(Employee t) {
		int value =	getSqlSession().insert( "Employee.insert", t );
		if (value == 1) { return true; }
		else { return false; }
	}

	public boolean merge(Employee t) {
		int value =	getSqlSession().update( "Employee.update", t );
		if (value == 1) { return true; }
		else { return false; }
	}

	public boolean delete(Employee t) {
		int value =	getSqlSession().delete( "Employee.deleteById", t );
		if (value == 1) { return true; }
		else { return false; }
	}

	public List<Employee> selectAll() {
		List<Employee> employees =	getSqlSession().selectList("Employee.getAll");
		return employees;
	}

	public Employee getObjectById(int id) {
		Employee  employee = getSqlSession().selectOne("Employee.getById", id);
		return employee;
	}

	@Override
	public List<Employee> getManagers() {
		List<Employee> employees =	getSqlSession().selectList("Employee.getManagers");
		return employees;
	}
	
}
