package com.company.daos;

import java.util.List;

import com.company.models.Employee;

public interface EmployeeDao extends DbDao<Employee> {
	public List<Employee> getManagers();
}
