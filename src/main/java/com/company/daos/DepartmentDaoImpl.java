package com.company.daos;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.company.models.Department;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class DepartmentDaoImpl implements DepartmentDao {

	@Inject
	private SqlSession sqlSession;
	
	
	public boolean save(Department t) {
		int value =	getSqlSession().insert( "Department.insert", t );
		if (value == 1) { return true; }
		else { return false; }
	}

	public boolean merge(Department t) {
		int value = getSqlSession().update("Department.update", t );
		if (value == 1) { return true; }
		else { return false; }
	}

	public boolean delete(Department t) {
		int value = getSqlSession().delete( "Department.deleteById", t.getDepartmentId() );
		if (value == 1) { return true; }
		else { return false; }
	}

	public List<Department> selectAll() {
        List<Department> depts = getSqlSession().selectList( "Department.getAll" );
        return depts;
	}

	public Department getObjectById(int id) {
        Department dept = ( Department ) getSqlSession().selectOne( "Department.getById", id );
        return dept;
	}

	private SqlSession getSqlSession() {
		return this.sqlSession;
	}
}
