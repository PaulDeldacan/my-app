package com.company.daos;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.company.models.Job;
import com.google.inject.Inject;

public class JobDaoImpl implements JobDao {

	@Inject
	private SqlSession sqlSession;
	
	public SqlSession getSqlSession() {
		return sqlSession;
	}

	public void setSqlSession(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}

	@Override
	public boolean save(Job t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean merge(Job t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Job t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Job> selectAll() {
		List<Job> jobs = getSqlSession().selectList("Job.getAll");
		if ( jobs != null ) { return jobs; } 
		else { return new ArrayList<Job>(); }
	}

	@Override
	public Job getObjectById(int id) {
		Job job = getSqlSession().selectOne("Job.getById", id);
		if ( job != null ) { return job; } 
		else { return new Job(); }
	}

}
