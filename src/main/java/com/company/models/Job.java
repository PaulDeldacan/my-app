package com.company.models;

import java.math.BigInteger;

public class Job {
	private String jobId;
	private String jobTittle;
	private BigInteger minSalary;
	private BigInteger maxSalary;

	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getJobTittle() {
		return jobTittle;
	}
	public void setJobTittle(String jobTitle) {
		this.jobTittle = jobTitle;
	}
	public BigInteger getMinSalary() {
		return minSalary;
	}
	public void setMinSalary(BigInteger minSalary) {
		this.minSalary = minSalary;
	}
	public BigInteger getMaxSalary() {
		return maxSalary;
	}
	public void setMaxSalary(BigInteger maxSalary) {
		this.maxSalary = maxSalary;
	}

}
