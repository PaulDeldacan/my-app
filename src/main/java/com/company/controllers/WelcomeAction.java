package com.company.controllers;

import com.company.interceptors.UserAware;
import com.company.models.User;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class WelcomeAction extends ActionSupport implements UserAware, ModelDriven<User>{

	private static final long serialVersionUID = -4623456036261691184L;
	private User user;
	
	public String execute() throws Exception{
		return "success";
	}

	@Override
	public User getModel() {
		return getUser();
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}
}
