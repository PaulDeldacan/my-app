package com.company.controllers;

import java.util.ArrayList;
import java.util.List;

import oracle.net.aso.i;

import com.company.models.Department;
import com.company.services.DepartmentService;
import com.google.inject.Inject;

public class DeptAction {
	
	private List<Department> departments;
	private Department department;
	private int id;
	private String action;
	
	@Inject
	private DepartmentService service;
	
	private DepartmentService getService(){
		return this.service;
	}
	
	public String execute() throws Exception{
		return "success";
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
		setDepartment( findDepartmentById(getId()) );
	}

	public List<Department> getDepartments() {
		return departments;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}
	
	public String loadDepartments(){
		setDepartments( getAllDepartments() );
		return "success";
	}
	
	private Department findDepartmentById( int id ){
		Department dept = getService().getObjectById( id ) ;
		return dept;
	}
	
	public String save(){
		boolean isSaved =  createDepartment( getDepartment() );
		if ( isSaved ) { return "success"; }
		else { return "error"; }
	}
	
	public String update(){
		boolean isUpdated = updateDepartment( getDepartment() ); 
		if ( isUpdated ) { return "success"; } 
		else { return "error"; }
	}
	
	public String delete(){
		boolean isDeleted = deleteDepartment( getDepartment() ); 
		if ( isDeleted ) { return "success"; } 
		else { return "error"; }
	}

	private List<Department>getAllDepartments(){
		List<Department> depts = getService().selectAll();
		if ( depts != null ) { return depts; }
		else{ return new ArrayList<Department>(); }
	}
	
	private boolean createDepartment( Department dept ){
		return getService().save( dept );
	}
	
	private boolean updateDepartment( Department dept ){
		return getService().merge( dept );
	}
	
	private boolean deleteDepartment( Department dept ) {
		return getService().delete( dept );
	}
}
