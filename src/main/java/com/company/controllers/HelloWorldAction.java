package com.company.controllers;

import com.company.actionsupport.Action;

public class HelloWorldAction implements Action {
	private String name;
	
	public String execute() throws Exception{
		if ( getName().equals("w") ) {
			return SUCCESS; 
		}else{
			return ERROR;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
