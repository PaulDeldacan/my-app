package com.company.controllers;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.company.models.User;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class LoginAction extends ActionSupport implements SessionAware, ModelDriven<User> {

	private static final long serialVersionUID = -7879992798426811967L;
	private User user;
	private Map<String, Object> sessionAttributes;
	
	{
		setUser( new User() );
	}
	
	
	@Override
	public String execute() throws Exception{
		System.out.println("login action executing...");
		if ( getUser().getUsername().equals( "paul" ) && getUser().getPassword().equals( "paul!" ) ) {
			System.out.println( "log in process done." );
			getUser().setOwnerName( "Paul Deldacan" );
			return SUCCESS;
		}
		System.out.println( "log in process done." );
		return INPUT;
	}
	
	@Override
	public User getModel() {
		return getUser();
	}

	@Override
	public void setSession(Map<String, Object> sessionAttributes) {
		setSessionAttributes( sessionAttributes );
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Map<String, Object> getSessionAttributes() {
		return sessionAttributes;
	}

	public void setSessionAttributes(Map<String, Object> sessionAttributes) {
		this.sessionAttributes = sessionAttributes;
	}
	
}
