package com.company.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.company.models.Department;
import com.company.models.Employee;
import com.company.models.Job;
import com.company.services.DepartmentService;
import com.company.services.EmployeeService;
import com.company.services.JobService;
import com.google.inject.Inject;

public class EmployeeAction {
	private List<Employee> employees;
	private Employee employee;
	private int id;
	private List<Department> departments;
	private List<Job> jobs;
	private List<Employee> managers;
	
	private EmployeeService employeeService;
	private DepartmentService departmentService;
	private JobService jobService;
	
	@Inject
	public EmployeeAction(EmployeeService employeeService, DepartmentService departmentService, JobService jobService) {
		setEmployeeService( employeeService );
		setDepartmentService( departmentService );
		setJobService( jobService );
		setDepartments( selectDepartments() );
		setJobs( selectJobs() );
		setManagers( selectManagers() );
	}

	public String execute() throws Exception{
		setDepartments( selectDepartments() );
		return "success";
	}
	
	private List<Department> selectDepartments() {
		return getDepartmentService().selectAll();
	}
	
	private List<Job> selectJobs(){
		return getJobService().selectAll();
	}
	
	private List<Employee> selectManagers(){
		return getEmployeeService().getManagers();
	}

	public List<Employee> getEmployees() {
		return employees;
	}
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
		setEmployee( findEmployeetById( id ) );
	}
	
	public String loadEmployees(){
		setEmployees( getAllEmployees() );
		return "success";
	}
	
	private Employee findEmployeetById( int id ){
		Employee dept = getEmployeeService().getObjectById( id ) ;
		return dept;
	}
	
	public String save(){
		boolean isSaved =  createEmployee( getEmployee() );
		if ( isSaved ) { return "success"; }
		else { return "error"; }
	}
	
	public String update(){
		boolean isUpdated = updateEmployee( getEmployee() ); 
		if ( isUpdated ) { return "success"; } 
		else { return "error"; }
	}
	
	public String delete(){
		System.out.println("deleting...");
		boolean isDeleted = deleteEmployee( getEmployee() ); 
		if ( isDeleted ) { return "success"; } 
		else { return "error"; }
	}

	private List<Employee>getAllEmployees(){
		List<Employee> employees = getEmployeeService().selectAll();
		if ( employees != null ) { return employees; }
		else{ return new ArrayList<Employee>(); }
	}
	
	private boolean createEmployee( Employee employee ){
		return getEmployeeService().save( employee );
	}
	
	private boolean updateEmployee( Employee employee ){
		return getEmployeeService().merge( employee );
	}
	
	private boolean deleteEmployee( Employee employee ) {
		return getEmployeeService().delete( employee );
	}
	public EmployeeService getEmployeeService() {
		return employeeService;
	}
	public void setEmployeeService(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	public Date getCurrentDate() {
		return new Date();
	}

	public DepartmentService getDepartmentService() {
		return departmentService;
	}

	public void setDepartmentService(DepartmentService departmentService) {
		this.departmentService = departmentService;
	}

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	public List<Job> getJobs() {
		return jobs;
	}

	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public List<Employee> getManagers() {
		return managers;
	}

	public void setManagers(List<Employee> managers) {
		this.managers = managers;
	}
	
}
