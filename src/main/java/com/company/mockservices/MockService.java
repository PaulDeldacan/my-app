package com.company.mockservices;

import java.util.List;

import com.company.daos.DepartmentDao;
import com.company.models.Department;

import javax.inject.Singleton;

@Singleton
public class MockService implements DepartmentDao{

	public boolean save(Department t) {
		System.out.println("test start saving...");
		if ( t != null ) {
			System.out.println("department to save: " + t.getDepartmentName());
			return true;
		}else{
			System.out.println("department is empty.");
			return false;
		}
	}

	public boolean merge(Department t) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean delete(Department t) {
		// TODO Auto-generated method stub
		return false;
	}

	public List<Department> selectAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public Department getObjectById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
