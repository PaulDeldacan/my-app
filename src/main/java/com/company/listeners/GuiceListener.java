package com.company.listeners;

import org.mybatis.guice.XMLMyBatisModule;

import com.company.injector.AppInjector;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;

public class GuiceListener extends GuiceServletContextListener{
	private Injector injector;

	
	@Override
	public Injector getInjector() {
		setInjector( Guice.createInjector(
				new AppInjector(),
				new com.google.inject.servlet.ServletModule(),
				new com.google.inject.struts2.Struts2GuicePluginModule(),
				new XMLMyBatisModule() {
					
					@Override
					protected void initialize() {
						setEnvironmentId("development");
						setClassPathResource("SqlMapConfig.xml");
					}
				}) );
		return injector;
	}

	private void setInjector(Injector injector) {
		this.injector = injector;
	}
	
}
