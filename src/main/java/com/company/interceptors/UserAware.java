package com.company.interceptors;

import com.company.models.User;

public interface UserAware {
	public void setUser( User user );
}
