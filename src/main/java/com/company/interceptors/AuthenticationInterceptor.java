package com.company.interceptors;

import java.util.Map;

import com.company.actionsupport.Action;
import com.company.models.User;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class AuthenticationInterceptor implements Interceptor{

	private static final long serialVersionUID = 1938329848762095564L;

	@Override
	public void destroy() { }

	@Override
	public void init() { }

	@Override
	public String intercept(ActionInvocation actionInvocation) throws Exception {
		System.out.println("intercepting...");
		Map<String, Object> sessionAttributes = actionInvocation.getInvocationContext().getSession();
		User user = ( User ) sessionAttributes.get( "USER" );
		
		if ( user == null ) {
			return Action.LOGIN;
		} else {
			Action action = ( Action ) actionInvocation.getAction();
			if ( action instanceof UserAware ) {
				( ( UserAware ) action ).setUser( user );
			}
			return actionInvocation.invoke();
		}
	}

}
