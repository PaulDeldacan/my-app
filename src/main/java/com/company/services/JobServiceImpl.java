package com.company.services;

import java.util.List;

import com.company.daos.JobDao;
import com.company.models.Job;
import com.google.inject.Inject;

public class JobServiceImpl implements JobService {

	@Inject
	private JobDao jobDao;
	
	public JobDao getJobDao() {
		return jobDao;
	}

	@Override
	public boolean save(Job t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean merge(Job t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Job t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Job> selectAll() {
		return getJobDao().selectAll();
	}

	@Override
	public Job getObjectById(int id) {
		return getJobDao().getObjectById( id );
	}

}
