package com.company.services;

import java.util.List;

import com.company.daos.EmployeeDao;
import com.company.models.Employee;
import com.google.inject.Inject;

public class EmployeeServiceImpl implements EmployeeService{

	@Inject
	private EmployeeDao employeeDao;
	
	private EmployeeDao getEmployeeDao(){
		return this.employeeDao;
	}
	
	@Override
	public boolean save(Employee t) {
		return getEmployeeDao().save( t );
	}

	@Override
	public boolean merge(Employee t) {
		return getEmployeeDao().merge( t );
	}

	@Override
	public boolean delete(Employee t) {
		return getEmployeeDao().delete( t );
	}

	@Override
	public List<Employee> selectAll() {
		return getEmployeeDao().selectAll();
	}

	@Override
	public Employee getObjectById(int id) {
		return getEmployeeDao().getObjectById( id );
	}

	@Override
	public List<Employee> getManagers() {
		return getEmployeeDao().getManagers();
	}

}
