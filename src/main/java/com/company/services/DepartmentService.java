package com.company.services;

import java.util.List;

import com.company.models.Department;

public interface DepartmentService extends AbstractService<Department> {
	boolean save(Department t);
	boolean merge(Department t);
	boolean delete(Department t); 
	List<Department> selectAll();
	Department getObjectById( int id );
}
