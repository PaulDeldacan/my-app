package com.company.services;

import java.util.List;

import com.company.daos.DepartmentDao;
import com.company.models.Department;
import com.google.inject.Inject;

public class DepartmentServiceImpl implements DepartmentService{

	@Inject
	private DepartmentDao deptDao;
	
	private DepartmentDao getDeptDao(){
		return this.deptDao;
	}
	
	@Override
	public boolean save(Department t) {
		return getDeptDao().save( t );
	}

	@Override
	public boolean merge(Department t) {
		return getDeptDao().merge( t );
	}

	@Override
	public boolean delete(Department t) {
		return getDeptDao().delete( t );
	}

	@Override
	public List<Department> selectAll() {
		return getDeptDao().selectAll();
	}

	@Override
	public Department getObjectById(int id) {
		return getDeptDao().getObjectById( id );
	}

}
