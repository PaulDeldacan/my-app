package com.company.services;

import java.util.List;

public interface AbstractService<T> {
	boolean save(T t);
	boolean merge(T t);
	boolean delete(T t); 
	List<T> selectAll();
	T getObjectById( int id );
}
