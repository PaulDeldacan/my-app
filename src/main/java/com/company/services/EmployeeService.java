package com.company.services;

import java.util.List;

import com.company.models.Employee;

public interface EmployeeService extends AbstractService<Employee>{
	public List<Employee> getManagers();
}
