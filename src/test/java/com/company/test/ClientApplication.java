package com.company.test;

import java.util.List;

import org.mybatis.guice.XMLMyBatisModule;

import com.company.consumer.DepartmentConsumer;
import com.company.injector.AppInjector;
import com.company.models.Department;
import com.google.inject.Guice;
import com.google.inject.Injector;

public class ClientApplication {
	private String test;
	private String test1;
	private String test2;
	public static void main( String args[] ){
	
		Injector injector = Guice.createInjector( new AppInjector(),  new XMLMyBatisModule() {
			@Override
			protected void initialize() {
				setEnvironmentId("development");
				setClassPathResource("SqlMapConfig.xml");
			}
		});
		
		DepartmentConsumer deptConsumer = injector.getInstance( DepartmentConsumer.class );
		
//		selectDepartments( deptConsumer );
		
//		Department dept = deptConsumer.getDeptById( 300 );
		
//		if ( dept != null ) {
//			dept.setDepartmentName("CPU Medicine_1");
//			updateDepartment( deptConsumer, dept );
//		}		
		
//		Department dept_1 = new Department();
//		dept_1.setDepartmentName( "CPU Medicine" );
		
//		createNewDepartment(deptConsumer, dept_1);
//		if ( dept != null ) {
//			deleteDepartment( deptConsumer, dept );
//		}
	}
	
	private static void selectDepartments( DepartmentConsumer deptConsumer ){
		List<Department> depts = deptConsumer.getAllDepartments();
		if ( deptConsumer.getAllDepartments() != null ) {
			for ( Department dept : depts  ) {
				System.out.println( "Dept #: " + dept.getDepartmentId() + " - " + "Dept. name: " + dept.getDepartmentName()  );
			}
		}else{
			System.out.println("No records of departments.");
		}
	}

	private static void updateDepartment(DepartmentConsumer deptConsumer,
			Department dept) {
		boolean isDeptUpdated = deptConsumer.updateDepartment( dept );
		if ( isDeptUpdated ) {
			System.out.println( "Dept: " + dept.getDepartmentName() + " has been updated.");
		} else {
			System.out.println("Dept: " + dept.getDepartmentName() + " is not updated." );
		}
	}

	private static void deleteDepartment(DepartmentConsumer deptConsumer,
			Department dept) {
		boolean isDeptDeleted = deptConsumer.deleteDepartment( dept );
		if ( isDeptDeleted ) {
			System.out.println( "Dept: " + dept.getDepartmentName() + " has been deleted.");
		} else {
			System.out.println("Dept: " + dept.getDepartmentName() + " is not deleted." );
		}
	}

	private static void createNewDepartment(DepartmentConsumer deptConsumer, Department dept) {
		boolean isDeptSaved = deptConsumer.saveDepartment( dept );
		if ( isDeptSaved ) {
			System.out.println( "Dept: " + dept.getDepartmentName() + " has been saved.");
		} else {
			System.out.println("Dept: " + dept.getDepartmentName() + " is not saved." );
		}
	}
}
