package com.company.test;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mybatis.guice.XMLMyBatisModule;

import com.company.consumer.DepartmentConsumer;
import com.company.injector.AppInjector;
import com.company.models.Department;
import com.google.inject.Guice;
import com.google.inject.Injector;

public class AppTest {
	private Injector injector2;	
 	private String test;	
	
	
	private void setInjector( Injector injector ){
		this.injector = injector;
	}
	
	private Injector getInjector(){
		return this.injector;
	}
	
	@Before
	public void setUp() throws Exception {
		setInjector( Guice.createInjector( new AppInjector(),  new XMLMyBatisModule() {
			@Override
			protected void initialize() {
				setEnvironmentId("development");
				setClassPathResource("SqlMapConfig.xml");
			}
		}));
	}
	
	@After
	public void tearDown() throws Exception{
		setInjector( null );
	}
	
	@Test
	public void testSelectAllDept(){
		DepartmentConsumer deptConsumer = getInjector().getInstance( DepartmentConsumer.class );
		List<Department> depts = deptConsumer.getAllDepartments();
		for (Department department : depts) {
			System.out.println("department: " + department.getDepartmentName());
		}
	}
	
	@Test
	public void testSelectDeptById(){
		DepartmentConsumer deptConsumer = getInjector().getInstance( DepartmentConsumer.class );
		Department dept = deptConsumer.getDeptById( 10 );
		System.out.println ("department with id 100: " + dept.getDepartmentName() );
	}
	
//	private Properties createTestProperties(){
//		final Properties myBatisProperties = new Properties();
//		myBatisProperties.setProperty("mybatis.environment.id", "test");
//		myBatisProperties.setProperty("JDBC.schema", "hr");
//		myBatisProperties.setProperty("JDBC.username", "hr");
//		myBatisProperties.setProperty("JDBC.password", "hr");
//		myBatisProperties.setProperty("JDBC.autoCommit", "false");
//		myBatisProperties.setProperty("oracle.sid", "XE");
//		return myBatisProperties;
//	}
	
//	@Test
//	public void test(){
//		DepartmentConsumer deptConsumer = getInjector().getInstance( DepartmentConsumer.class );
//		
//		Department dept = new Department();
//		dept.setDepartmentName( "Engineering" );
//		
//		Assert.assertEquals( true , deptConsumer.saveDepartment( dept ));
//	}
}
	
